package com.madrid.pollution.Util;

import com.madrid.pollution.Mappers.Dato;
import com.madrid.pollution.Repository.CouchDB_Handler;
import net.sf.json.JSONObject;
import net.sf.json.xml.XMLSerializer;
import org.ektorp.DocumentNotFoundException;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class DataHandler extends Thread {

    private BufferedReader datos;

    private CouchDB_Handler ch = CouchDB_Handler.getCouchDBInstance();

    /**
     *
     */
    public DataHandler() {


    }

    /**
     * Recupera los datos de la API y los setea en la Base de Datos
     */
    private void peticionDatos() {
        //Recuperamos los datos de la API
        datos = ch.retrieveDataFromAPI(Labels.API);

        // Construimos el XML recuperados de la API
        buildXml();

    }


    /**
     * Construye el documento XML a nuestro gusto para parsearlo
     * a posteriormente a JSON.
     */
    private void buildXml() {
        StringBuilder xml = new StringBuilder();
        boolean firstTime = true;
        String linea;
        String dia, mes = null, ano = null, fecha;
        String[] key;
        String cabecera, estacion = null, magnitud = null;
        try {
            cabecera = datos.readLine();
            while ((linea = datos.readLine()) != null) {
                key = linea.split("<")[1].split(">");
                if (key[0].startsWith("H") && firstTime) {
                    firstTime = false;
                    xml.append("\t<valores>" + "\n");
                }

                if (key[0].startsWith("Dato_Horario"))
                    xml.append(linea).append("\n");
                if (key[0].equals("estacion") || key[0].equals("magnitud")) {
                    if (key[0].equals("estacion"))
                        estacion = key[1];
                    if (key[0].equals("magnitud"))
                        magnitud = key[1];
                    xml.append(linea).append("\n");
                }

                if (key[0].equals("ano")) {
                    ano = key[1];
                }

                if (key[0].equals("mes")) {
                    mes = key[1];
                }

                if (key[0].equals("dia")) {
                    dia = key[1];
                    fecha = dia + "/" + mes + "/" + ano;
                    xml.append("\t<fecha>").append(fecha).append("</fecha>\n");
                }

                if (key[0].equals("/Dato_Horario")) {
                    firstTime = true;
                    xml.append("\t</valores>" + "\n").append("</Dato_Horario>\n");
                    //Creamos el Dato...
                    xml.insert(0, cabecera).append("\n");
                    crearDocumento(xml, estacion, magnitud);
                    xml = new StringBuilder(); //Vaciamos el actual
                }
                if (!firstTime) {
                    if (!key[0].startsWith("V"))
                        xml.append("\t\t<value>").append(Double.parseDouble(key[1])).append("</value>\n");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Crea o actualiza un documento JSON de la base de datos.
     * Para ello, debemos convertir el XML a JSON, ya que la BBDD usa
     * únicamente ficheros JSON.
     * <p>
     * Si ya existe el documento, lo actualizamos, si no, lo creamos.
     *
     * @param xml      documento xml a parsear a JSOn
     * @param estacion estación para construir el id
     * @param magnitud magnitud para construir el ID
     */
    private void crearDocumento(StringBuilder xml, String estacion, String magnitud) {

        XMLSerializer xmlSerializer = new XMLSerializer();

        //Y lo convertimos a un objeto JSON
        JSONObject json = (JSONObject) xmlSerializer.read(String.valueOf(xml));

        String id = ch.getCurrentDate() + "_" + estacion + "_" + magnitud;
        try {
            Dato dato = ch.getDb().get(Dato.class, id);

            //Si el dato existe en la base de datos, lo actualizamos, si no, creamos uno nuevo.
            if (dato != null) {
                System.out.println(Labels.INFO_MSG + " Modificando el documento " + id + ".");
                if (ch.updateDocument(id, dato.getRevision(), json))
                    System.out.println(Labels.OK_MSG + " Documento " + id + " actualizado.");
                else
                    System.out.println(Labels.ERR_MSG + " El documento " + id + " ha podido actualizarse.");
            } else {
                System.out.println(Labels.INFO_MSG + " Creando el documento " + id + " en la Base de Datos.");
                ch.getDb().create(id, json);
            }
            //Controlamos la excepción de que no exista el documento la primera vez que lanzamos la aplicación
        } catch (DocumentNotFoundException e) {
            System.out.println(Labels.WARN_MSG + " El documento " + id + " no existe en la base de datos.");
            System.out.println(Labels.INFO_MSG + " Creando el documento " + id + " en la Base de Datos.");
            ch.getDb().create(id, json);
            System.out.println(Labels.OK_MSG + " Documento" + id + "creado.");
        }
    }

    /**
     * Crea un timer desde el instante actual y se ejecuta cada hora para
     * pedir los datos a la API y actualizarlos o crearlos de nuevo
     */
    public void run() {
        Calendar c = Calendar.getInstance();
        Date horaRepeticion = c.getTime();

        int tiempoRepeticion = 3600000;
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                System.out.println(Labels.INFO_MSG + "Ejecuntando tarea...");
                peticionDatos();
            }
        };
        timer.schedule(task, horaRepeticion, tiempoRepeticion);

    }
}
