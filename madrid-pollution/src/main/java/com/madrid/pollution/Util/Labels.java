package com.madrid.pollution.Util;

public class Labels {

    /*Datos de la base de datos*/
    public static final String DB_NAME = "madrid-pollution";
    public static final String URL = "http://localhost";
    public static final String PORT = "5984";
    public static final String USERNAME = "admin";
    public static final String PASS = "admin";


    /*Tipos de mensaje*/
    public static final String INFO_MSG = "[INFO]";
    public static final String WARN_MSG = "[WARNING]";
    public static final String ERR_MSG = "[ERROR]";
    public static final String OK_MSG = "[OK]";

    /*API data*/
    public static final String API = "http://www.mambiente.madrid.es/opendata/horario.xml";


    /*DATOS ESTACIONES*/
    public static final String E004 = "Pza. de España";
    public static final String E008 = "Escuelas Aguirre";
    public static final String E011 = "Avda. Ramón y Cajal";
    public static final String E016 = "Arturo Soria";
    public static final String E017 = "Villaverde";
    public static final String E018 = "Farolillo";
    public static final String E024 = "Casa de Campo";
    public static final String E027 = "Barajas Pueblo";
    public static final String E035 = "Pza. del Carmen";
    public static final String E036 = "Moratalaz";
    public static final String E038 = "Cuatro Caminos";
    public static final String E039 = "Barrio del Pilar";
    public static final String E040 = "Vallecas";
    public static final String E047 = "Mendez Alvaro";
    public static final String E048 = "Castellana";
    public static final String E049 = "Parque del Retiro";
    public static final String E050 = "Plaza Castilla";
    public static final String E054 = "Ensanche de Vallecas";
    public static final String E055 = "Urb. Embajada";
    public static final String E056 = "Pza. Fernández Ladreda";
    public static final String E057 = "Sanchinarro";
    public static final String E058 = "El Pardo";
    public static final String E059 = "Juan Carlos I";
    public static final String E060 = "Tres Olivos";


    /*DATOS MAGNITUDES*/
    public static final String SO = "Dióxido de Azufre";
    public static final String CO = "Monóxido de Carbono";
    public static final String NO = "Monóxido de Nitrógeno";
    public static final String NO2 = "Dióxido de Nitrógeno";
    public static final String PM2_5 = "Partículas < 2.5 μm";
    public static final String PM10 = "Partículas < 10 μm";
    public static final String NOx = "Óxidos de Nitrógeno";
    public static final String O3 = "Ozono";
    public static final String TOL = "Tolueno";
    public static final String BEN = "Benceno";
    public static final String EBE = "Etilbenceno";
    public static final String MXY = "Metaxileno";
    public static final String PXY = "Paraxileno";
    public static final String OXY = "Ortoxileno";
    public static final String TCH = "Hidrocarburos totales (hexano)";
    public static final String CH4 = "Metano";
    public static final String NMHC = "Hidrocarburosno metánicos (hexano)";

    /*UNIDADES MEDIDA*/
    public static final String SO_MED = "μg/m³";
    public static final String CO_MED = "mg/m³";
    public static final String NO_MED = "μg/m³";
    public static final String NO2_MED = "μg/m³";
    public static final String PM2_5_MED = "μg/m³";
    public static final String PM10_MED = "μg/m³";
    public static final String NOx_MED = "μg/m³";
    public static final String O3_MED = "μg/m³";
    public static final String TOL_MED = "μg/m³";
    public static final String BEN_MED = "μg/m³";
    public static final String EBE_MED = "μg/m³";
    public static final String MXY_MED = "μg/m³";
    public static final String PXY_MED = "μg/m³";
    public static final String OXY_MED = "μg/m³";
    public static final String TCH_MED = "mg/m³";
    public static final String CH4_MED = "mg/m³";
    public static final String NMHC_MED = "mg/m³";

    /*CONSULTAS A LA API*/
    public static final String VISTAS = "http://127.0.0.1:5984/madrid-pollution/_design/vistas/_view/";

    /**
     * Dado el código de una estación, muestra la magnitud, el día y los valores de la estación
     *
     * La clave es estacion
     */
    public static final String DATOS_ESTACIONES_DIA = "estaciones";

    /**
     * Dada una magnitud, recupera la suma total del máximo de los valores tomado de todas las estaciones
     * de un día o rango dado
     * <p>
     * La clave está compuesta por [magnitud, estacion]
     */
    public static final String SUMA_MAGN_MAX_X_ESTACION = "sumaMagnitudesMaximasPorEstacion";
    public static final String VALORES_MAX_X_FECHA_Y_MAGN = "valoresMaximosPorFechaYMagnitud";
    public static final String VALORES_X_MAGN = "valoresPorMagnitud";
    public static final String MEDIANA = "mediana";
    public static final String MEDIA = "media";
    public static final String MEDIA_X_DIA = "mediaPorDia";
    public static final String MEDIA_X_DIA_Y_MAGNITUD = "mediaPorDiaYMagnitud";
    public static final String MEDIA_POR_ESTACION_Y_DIA = "mediaPorEstacionYDia";
    public static final String MEDIA_POR_ESTACION = "mediaPorEstacion";
    public static final String MAGNITUDES_MAXIMAS = "magnitudesMaximas";

    public static final String START_KEY = "startkey";
    public static final String END_KEY = "endkey";
    public static final String KEY = "key";
}
