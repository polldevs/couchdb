package com.madrid.pollution.Util;

import java.util.List;

public class DataAnalytics {

    public DataAnalytics() {

    }

    /**
     * Calcula la media de entre todos los elementos tomados
     *
     * @return
     */
    public Double calcularMedia(List<Double> list) {
        Double avg = 0.0;
        for (Double valor : list) {
            avg += valor;
        }
        return (avg / list.size());
    }


    /**
     * Calcula la mediana de una lista dada.
     *
     * @return
     */
    public Double calcularMEdiana(List<Double> list) {
        int middle = list.size()/2;

        if (list.size() % 2 == 1)
            return list.get(middle);
        else
            return (list.get(middle-1)+ list.get(middle)) / 2.0;
    }

    /**
     * Calcula la desviación típica para cualquier lista de valores dado
     * @param list
     * @return
     */
    public Double calcularDesviacionTipica(List<Double> list) {
        int suma = 0;
        double media = calcularMedia(list);

        for (Double valor: list)
            suma += Math.pow((valor - media), 2);
        return Math.sqrt(suma / (list.size() -1));
    }

}
