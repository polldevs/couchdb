package com.madrid.pollution.Repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.madrid.pollution.Mappers.Dato;
import com.madrid.pollution.Util.Labels;
import net.sf.json.JSONObject;
import org.ektorp.CouchDbConnector;
import org.ektorp.CouchDbInstance;
import org.ektorp.http.HttpClient;
import org.ektorp.http.StdHttpClient;
import org.ektorp.impl.StdCouchDbConnector;
import org.ektorp.impl.StdCouchDbInstance;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CouchDB_Handler {

    private CouchDbInstance dbInstance;
    private CouchDbConnector db;
    private static CouchDB_Handler couchDBInstance;
    private BufferedReader datos;

    /**
     * Constructor privado del patrón singleton
     */
    private CouchDB_Handler() {
        createConection();
    }

    /**
     * Crea de manera sincronizada una instancia de la clase si no existe previamente
     */
    private synchronized static void createInstance() {
        if (couchDBInstance == null)
            couchDBInstance = new CouchDB_Handler();
    }

    /**
     * Crea una instancia de la clase si no existe
     *
     * @return devuelve la instancia de la clase
     */
    public static CouchDB_Handler getCouchDBInstance() {
        if (couchDBInstance == null)
            createInstance();
        return couchDBInstance;
    }

    /**
     * Crea y establece la conexión con la Base de Datos CouchDB
     */
    private void createConection() {
        try {
            HttpClient httpClient = new StdHttpClient.Builder()
                    .url(Labels.URL + ":" + Labels.PORT)
                    .username(Labels.USERNAME)
                    .password(Labels.PASS)
                    .build();

            dbInstance = new StdCouchDbInstance(httpClient);
            db = new StdCouchDbConnector(Labels.DB_NAME, dbInstance);

            //Si no existe, lo creamos
            db.createDatabaseIfNotExists();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

    }

    /**
     * Actualiza el documento cuya id coincida con el parámetro dado.
     *
     * @param id   : identificador del documento
     * @param rev  : última revisión del documento
     * @param json : Objeto JSON a actualizar
     * @return true si no ha habido error en el update, false si lo ha habido
     */
    public boolean updateDocument(String id, String rev, JSONObject json) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            Dato dato = mapper.readValue(json.toString(), Dato.class); //Convertimos el json a un objeto de tipo Dato
            /*Seteamos el id recibido y la última revisión del documento*/
            dato.setId(id);
            dato.setRevision(rev);

            //Actualizamos el dato...
            db.update(dato);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public CouchDbConnector getDb() {
        return db;
    }

    /**
     * Este método comprueba si hay documentos en el repositorio. Si los hay,retorna true.
     *
     * @return true si el repositorio no tiene documentos, false si tiene.
     */
    public boolean repositoryEmpty() {
        return db.getAllDocIds().isEmpty();
    }

    /**
     * Hace una petición GET a la url de la API y almacena el resultado en un objeto Buffer
     *
     * @return devuele el resultado en un objeto BufferedReader
     */
    public BufferedReader retrieveDataFromAPI(String uri) {
        try {
            // Creando un objeto URL
            URL url = new URL(uri);

            // Realizando la petición GET
            URLConnection con = url.openConnection();

            // Leyendo el resultado
            return new BufferedReader(new InputStreamReader(
                    con.getInputStream()));

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * Consulta una vista dada por parámetro de la base de datos.
     * Puede tener parámetros de manera opcional para obtener rango de valores
     *
     * @param vista vista a consultar
     * @param startkey parámetro de comienzo
     * @param endkey parámetro de fin
     */
    public BufferedReader consultarVista(String vista, String startkey, String endkey) {
        String URI = Labels.VISTAS + vista;
        if (startkey != null && endkey != null) {
            URI = URI + "?" + Labels.START_KEY + "=" + startkey + "&" + Labels.END_KEY + "=" + endkey;
        } else if (startkey != null)
            URI = URI + "?" + Labels.KEY + "=" + startkey;
        datos = retrieveDataFromAPI(URI);
        mostrarResultado();
        return datos;
    }

    /**
     * Muestra el resultado de los datos
     */
    public void mostrarResultado() {
        String linea;
        try {
            if (datos != null) {
                while ((linea = datos.readLine()) != null) {
                    System.out.println(linea);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Construye la fecha actual con el formato dd/MM/yyyy
     *
     * @return devuelve la fecha actual como String.
     */
    public String getCurrentDate() {
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date fechaActual = new Date();
        return format.format(fechaActual);
    }


}
