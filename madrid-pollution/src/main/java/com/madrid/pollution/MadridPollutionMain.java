package com.madrid.pollution;

import com.madrid.pollution.UI.UI;
import com.madrid.pollution.Util.DataHandler;

public class MadridPollutionMain {

    public static void main(String[] args) {
        UI interfaz = new UI();
        DataHandler dataHandler = new DataHandler();

        dataHandler.start(); //Cargamos los datos...
        try {
            //Esperamos a que termine la ejecución del primer hilo
            dataHandler.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //LLamamos al segundo hilo cuando termine el primero
        interfaz.start();
    }
}
