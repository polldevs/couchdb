package com.madrid.pollution.Entities;

import java.util.List;

//TODO Utilizar la clase para almacenar entidades al recuperar los datos de la API
public class Estacion {
    private int codigo;

    private String nombre;

    private List<Magnitud> magnitudes;

    public Estacion(int codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
    }

    public Estacion() {
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Magnitud> getMagnitudes() {
        return magnitudes;
    }

    public void setMagnitudes(List<Magnitud> magnitudes) {
        this.magnitudes = magnitudes;
    }
}
