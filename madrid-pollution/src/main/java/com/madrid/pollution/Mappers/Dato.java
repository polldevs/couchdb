package com.madrid.pollution.Mappers;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.ektorp.support.CouchDbDocument;

import java.util.ArrayList;
import java.util.List;

public class Dato extends CouchDbDocument {

    @JsonProperty("_id")
    private String id;

    @JsonProperty("_rev")
    private String revision;

    @JsonProperty("estacion")
    private int estacion;

    @JsonProperty("magnitud")
    private int magnitud;

    @JsonProperty("fecha")
    private String fecha;

    @JsonProperty("valores")
    private List<Double> valores;

    public Dato(String id, String revision, int estacion, int magnitud, String fecha, List<Double> valores) {
        this.id = id;
        this.revision = revision;
        this.estacion = estacion;
        this.magnitud = magnitud;
        this.fecha = fecha;
        this.valores = valores;
    }

    public Dato() {
        valores = new ArrayList<Double>();
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getRevision() {
        return revision;
    }

    @Override
    public void setRevision(String revision) {
        this.revision = revision;
    }

    public int getEstacion() {
        return estacion;
    }

    public void setEstacion(int estacion) {
        this.estacion = estacion;
    }

    public int getMagnitud() {
        return magnitud;
    }

    public void setMagnitud(int magnitud) {
        this.magnitud = magnitud;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public List<Double> getValores() {
        return valores;
    }

    public void setValores(List<Double> valores) {
        this.valores = valores;
    }
}
