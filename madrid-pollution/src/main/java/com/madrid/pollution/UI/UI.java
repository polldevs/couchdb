package com.madrid.pollution.UI;

import com.madrid.pollution.Repository.CouchDB_Handler;
import com.madrid.pollution.Util.Labels;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class UI extends Thread {

    private static final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    private CouchDB_Handler couchDB_handler = CouchDB_Handler.getCouchDBInstance();

    public int crearMenu() {
        int input = 0;
        do {
            System.out.printf("\nMenú principal >>>\n" +
                    "\n\t1. Consultar estaciones." +
                    "\n\t2. Consultar promedio de polución." +
                    "\n\t3. Consultar valores máximos." +
                    "\n\n< 0. Salir." +
                    "\n\nInput > "
            );
            try {
                input = Integer.parseInt(br.readLine());
                if (input < 0 || input > 3)
                    System.out.println(Labels.ERR_MSG + " Número fuera de rango. Valores entre 1 y 3.");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } while (input < 0 || input > 3);
        return input;
    }

    public int seleccionaEstacion(int valor) {
        switch (valor) {
            case 1:
                return 4;
            case 2:
                return 8;
            case 3:
                return 11;
            case 4:
                return 16;
            case 5:
                return 17;
            case 6:
                return 18;
            case 7:
                return 24;
            case 8:
                return 27;
            case 9:
                return 35;
            case 10:
                return 36;
            case 11:
                return 38;
            case 12:
                return 40;
            case 13:
                return 47;
            case 14:
                return 48;
            case 15:
                return 49;
            case 16:
                return 50;
            case 17:
                return 54;
            case 18:
                return 55;
            case 19:
                return 56;
            case 20:
                return 57;
            case 21:
                return 58;
            case 22:
                return 59;
            case 23:
                return 60;
            case 0:
                System.out.println("Volviendo...");
                break;
        }
        return valor;
    }

    private int menuSeleccionarEstacion() {
        int input = 0;
        do {
            System.out.printf("Selecciona una estación >>>\n" +
                    "\n\t1. " + Labels.E004 +
                    "\n\t2. " + Labels.E008 +
                    "\n\t3. " + Labels.E011 +
                    "\n\t4. " + Labels.E016 +
                    "\n\t5. " + Labels.E017 +
                    "\n\t6. " + Labels.E018 +
                    "\n\t7. " + Labels.E024 +
                    "\n\t8. " + Labels.E027 +
                    "\n\t9. " + Labels.E035 +
                    "\n\t10. " + Labels.E036 +
                    "\n\t11. " + Labels.E038 +
                    "\n\t12. " + Labels.E040 +
                    "\n\t13. " + Labels.E047 +
                    "\n\t14. " + Labels.E048 +
                    "\n\t15. " + Labels.E049 +
                    "\n\t16. " + Labels.E050 +
                    "\n\t17. " + Labels.E054 +
                    "\n\t18. " + Labels.E055 +
                    "\n\t19. " + Labels.E056 +
                    "\n\t20. " + Labels.E057 +
                    "\n\t21. " + Labels.E058 +
                    "\n\t22. " + Labels.E059 +
                    "\n\t23. " + Labels.E060 +
                    "\n\n< 0. Atrás" +
                    "\n\nInput > "
            );
            try {
                input = Integer.parseInt(br.readLine());
                if (input < 0 || input > 23)
                    System.out.println(Labels.ERR_MSG + " Número fuera de rango. Valores entre 1 y 7.");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } while (input < 0 || input > 23);


        return input;
    }


    public int consultarPromedioPolucion() {
        int input = 0;
        do {
            System.out.printf("Selecciona una opción >>>\n" +
                    "\n\t1. Por día" +
                    "\n\t2. Por estación" +
                    "\n\t3. Por día y estación" +
                    "\n\t4. Por día y magnitud" +
                    "\n\t5. Histórico mediana por magnitud" +
                    "\n\t6. Histórico medio por magnitud" +
                    "\n\n< 0. Atrás" +
                    "\n\nInput > "
            );
            try {
                input = Integer.parseInt(br.readLine());
                if (input < 0 || input > 6)
                    System.out.println(Labels.ERR_MSG + " Número fuera de rango. Valores entre 1 y 7.");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } while (input < 0 || input > 6);
        return input;
    }

    public void ejecutarMenu(int opcion) {
        switch (opcion) {
            case 1:
                couchDB_handler.consultarVista(Labels.DATOS_ESTACIONES_DIA, "4", "60");
                break;
            case 2:
                int input;
                do {
                    input = consultarPromedioPolucion();
                    ejecutarPromedioPolucion(input);
                } while (input != 0);

                break;
            case 3:
                int input2;
                int magnitud;
                do {
                    input2 = menuSeleccionarMagnitud();
                    magnitud = seleccionarMagnitud(input2);
                } while (input2 < 0 || input2 > 17);

                if (input2 != 0)
                    do {
                        input2 = opcionesMaximosPolucion();
                        ejecutarOpcionesMaximosPolucion(input2, magnitud);
                    } while (input2 != 0);


            case 0:
                System.out.printf(Labels.INFO_MSG + " Saliendo de la aplicación");
                try {
                    for (int i = 0; i < 3; i++) {
                        sleep(1000);
                        System.out.printf(" .");
                    }
                    System.out.println("\n" + Labels.OK_MSG + " DESCONECTADO");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    public void ejecutarPromedioPolucion(int opcion) {
        int input2, magnitud, estacion;
        String fecha, nomMagnitud;
        switch (opcion) {
            case 1://MEDIA POR DÍA
                fecha = seleccionarFecha();
                System.out.println(Labels.INFO_MSG + " Consultando el promedio de valores de todas las magnitudes para el día " + fecha + "...");
                couchDB_handler.consultarVista(Labels.MEDIA_X_DIA, "\"" + fecha + "\"", null);
                System.out.println(Labels.OK_MSG + " Ok");
                break;
            case 2://MEDIA POR ESTACIÓN
                input2 = menuSeleccionarEstacion();
                estacion = seleccionaEstacion(input2);
                System.out.println(Labels.INFO_MSG + " Consultando el valor promedio de valores de la estacion " + estacion + "...");
                if (estacion != 0)
                    couchDB_handler.consultarVista(Labels.MEDIA_POR_ESTACION, "" + estacion, null);

                System.out.println(Labels.OK_MSG + " Ok");
                break;
            case 3://MEDIA POR DÍA Y ESTACIÓN
                fecha = seleccionarFecha();
                input2 = menuSeleccionarEstacion();
                estacion = seleccionaEstacion(input2);
                System.out.println(Labels.INFO_MSG + " Consultando el valor promedio de valores de la estacion " + estacion + " el día" + fecha + "...");
                if (estacion != 0)
                    couchDB_handler.consultarVista(Labels.MEDIA_POR_ESTACION_Y_DIA, "[\"" + fecha + "\"," + estacion + "]", null);
                System.out.println(Labels.OK_MSG + " Ok");
                break;
            case 4: //MEDIA POR DÍA Y MAGNITUD
                input2 = menuSeleccionarMagnitud();
                magnitud = seleccionarMagnitud(input2);
                nomMagnitud = devolverMagnitud(magnitud);
                fecha = seleccionarFecha();
                System.out.println(Labels.INFO_MSG + " Consultando el promedio de valores de " + nomMagnitud + " para el día " + fecha + "...");
                couchDB_handler.consultarVista(Labels.MEDIA_X_DIA_Y_MAGNITUD, "[\"" + fecha + "\"," + magnitud + "]", null);
                System.out.println(Labels.OK_MSG + " Ok");
                break;
            case 5://HISTÓRICO DE MEDIANA POR MAGNITUD
                input2 = menuSeleccionarMagnitud();
                magnitud = seleccionarMagnitud(input2);
                nomMagnitud = devolverMagnitud(magnitud);
                System.out.println(Labels.INFO_MSG + " Consultando la mediana histórica de la magnitud " + nomMagnitud+"...");
                couchDB_handler.consultarVista(Labels.MEDIANA, "" + magnitud, null);
                System.out.println(Labels.OK_MSG + " Ok");
                break;
            case 6: //HISTÓRICO DE MEDIA POR MAGNITUD
                input2 = menuSeleccionarMagnitud();
                magnitud = seleccionarMagnitud(input2);
                nomMagnitud = devolverMagnitud(magnitud);
                System.out.println(Labels.INFO_MSG + " Consultando el histórico de promedio de valores de la magnitude "+nomMagnitud+ "...");
                couchDB_handler.consultarVista(Labels.MEDIA, "" + magnitud, null);
                System.out.println(Labels.OK_MSG + " Ok");
                break;
            case 0:
                System.out.println(Labels.INFO_MSG + " Volviendo...");
                break;
        }
    }

    private int seleccionarMagnitud(int input2) {

        switch (input2) {
            case 1:
                return 1;
            case 2:
                return 6;

            case 3:
                return 7;

            case 4:
                return 8;

            case 5:
                return 9;

            case 6:
                return 10;

            case 7:
                return 12;

            case 8:
                return 14;

            case 9:
                return 20;

            case 10:
                return 30;

            case 11:
                return 35;

            case 12:
                return 37;

            case 13:
                return 38;

            case 14:
                return 39;

            case 15:
                return 42;

            case 16:
                return 43;

            case 17:
                return 44;

            case 0:
                System.out.println("Volviendo...");
                break;
        }
        return 0;
    }

    private int menuSeleccionarMagnitud() {
        int input = 0;
        do {
            System.out.printf("Selecciona una magnitud >>>\n" +
                    "\n\t1. " + Labels.SO +
                    "\n\t2. " + Labels.CO +
                    "\n\t3. " + Labels.NO +
                    "\n\t4. " + Labels.NO2 +
                    "\n\t5. " + Labels.PM2_5 +
                    "\n\t6. " + Labels.PM10 +
                    "\n\t7. " + Labels.NOx +
                    "\n\t8. " + Labels.O3 +
                    "\n\t9. " + Labels.TOL +
                    "\n\t10. " + Labels.BEN +
                    "\n\t11. " + Labels.EBE +
                    "\n\t12. " + Labels.MXY +
                    "\n\t13. " + Labels.PXY +
                    "\n\t14. " + Labels.OXY +
                    "\n\t15. " + Labels.TCH +
                    "\n\t16. " + Labels.CH4 +
                    "\n\t17. " + Labels.NMHC +
                    "\n\n< 0. Atrás" +
                    "\n\nInput > "
            );
            try {
                input = Integer.parseInt(br.readLine());
                if (input < 0 || input > 17)
                    System.out.println(Labels.ERR_MSG + " Número fuera de rango. Valores entre 0 y 17.");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } while (input < 0 || input > 17);


        return input;
    }

    private int opcionesMaximosPolucion() {
        int input = 0;
        do {
            System.out.println("¿Desde cuándo quiere recuperar esos valores? >>>\n\n" +
                    "\n\t1. Histórico" +
                    "\n\t2. Hoy" +
                    "\n\t3. En una fecha determinada" +
                    "\n\t4. En un rango de fecha" +
                    "\n\n< 0. Atrás" +
                    "n\nInput > ");
            try {
                input = Integer.parseInt(br.readLine());
                if (input < 0 || input > 4)
                    System.out.println(Labels.ERR_MSG + " Número fuera de rango. Valores entre 0 y 4.");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } while (input < 0 || input > 4);

        return input;
    }

    private void ejecutarOpcionesMaximosPolucion(int opcion, int magnitud) {
        String nomMagnitud = devolverMagnitud(magnitud);
        switch (opcion) {
            case 1: //HISTÓRICO
                System.out.println(Labels.INFO_MSG + " Consultando el histórico de máximos de " + nomMagnitud + "...");
                couchDB_handler.consultarVista(Labels.MAGNITUDES_MAXIMAS, "" + magnitud + "", null);
                System.out.println(Labels.OK_MSG + " Ok");

                break;
            case 2: //HOY
                System.out.println(Labels.INFO_MSG + " Consultando los valores máximos de hoy de " + nomMagnitud + "...");
                couchDB_handler.consultarVista(Labels.VALORES_MAX_X_FECHA_Y_MAGN, "[\"" + couchDB_handler.getCurrentDate() +
                        "\"," + magnitud + "]", null);
                System.out.println(Labels.OK_MSG + " Ok");

                break;
            case 3://DESDE UNA FECHA HASTA HOY
                String fecha = seleccionarFecha();
                System.out.println(Labels.INFO_MSG + " Consultando los valores máximos del" + nomMagnitud + "en el " + fecha);
                couchDB_handler.consultarVista(Labels.VALORES_MAX_X_FECHA_Y_MAGN, "[\"" + fecha + "\"," + magnitud +
                        "]", null);
                System.out.println(Labels.OK_MSG + " Ok");

                break;
            case 4: //RANGO DE FECHAS
                String fecha1 = seleccionarFecha();
                String fecha2 = seleccionarFecha();
                System.out.println(Labels.INFO_MSG + " Consultando los valores máximos de " + nomMagnitud + " entre " + fecha1 + " y " + fecha2 + "...");
                couchDB_handler.consultarVista(Labels.VALORES_MAX_X_FECHA_Y_MAGN, "[\"" + fecha1 + "\"," + magnitud +
                        "]", "[\"" + fecha2 + "\"," + magnitud + "]");

                System.out.println(Labels.OK_MSG + " Ok");
                break;
            case 0:
                System.out.println("Volviendo...");
                break;
        }
    }

    private String seleccionarFecha() {
        int dia = 0, mes = 0, ano = 0;
        boolean construido = false;
        String fecha = null;
        String fechaActual = couchDB_handler.getCurrentDate();
        String mesString = null;
        try {
            do {
                //Seleccionar día
                do {
                    System.out.printf("\n\t> Día: ");
                    dia = Integer.parseInt(br.readLine());
                    if (dia <= 0 || dia > 31)
                        System.out.println("Introduce un día válido");
                } while (dia <= 0 || dia > 31);

                //Seleccionar mes
                do {
                    System.out.printf("\n\t> Mes: ");
                    mes = Integer.parseInt(br.readLine());
                    if (mes <= 0 || mes > 12)
                        System.out.println("Introduce un mes válido");
                } while (mes <= 0 || mes > 12);

                //Seleccionar año
                int actualYear = Integer.parseInt(fechaActual.split("/")[2]);
                do {
                    System.out.printf("\n\t> Año: ");
                    ano = Integer.parseInt(br.readLine());
                    if (ano <= 1900 || ano > actualYear)
                        System.out.println("Introduce un año válido");
                } while (ano <= 1900 || ano > actualYear);

                //Comprobar que la fecha construida no es mayor que la fecha actual
                if (mes < 10) {
                    mesString = "0" + mes;
                }
                fecha = dia + "/" + mesString + "/" + ano;
                if (ano == actualYear) {
                    int actualMonth = Integer.parseInt(fechaActual.split("/")[1]);
                    if (mes < actualMonth) {
                        construido = true;
                    } else {
                        if (mes == actualMonth) {
                            int actualDay = Integer.parseInt(fechaActual.split("/")[0]);
                            if (dia <= actualDay) {
                                construido = true;
                            } else
                                System.out.println("La fecha no puede superar a la fecha actual");
                        } else
                            System.out.println("La fecha no puede superar a la fecha actual");
                    }
                } else
                    construido = true;
            } while (!construido);
        } catch (IOException e) {
            e.printStackTrace();
        }


        return fecha;
    }

    public String devolverMagnitud(int magnitud) {
        switch (magnitud) {
            case 1:
                return Labels.SO;
            case 6:
                return Labels.CO;
            case 7:
                return Labels.NO;
            case 8:
                return Labels.NO2;
            case 9:
                return Labels.PM2_5;
            case 10:
                return Labels.PM10;
            case 12:
                return Labels.NOx;
            case 14:
                return Labels.O3;
            case 20:
                return Labels.TOL;
            case 30:
                return Labels.BEN;
            case 35:
                return Labels.EBE;
            case 37:
                return Labels.MXY;
            case 38:
                return Labels.PXY;
            case 39:
                return Labels.OXY;
            case 42:
                return Labels.TCH;
            case 43:
                return Labels.CH4;
            case 44:
                return Labels.NMHC;
        }
        return null;
    }

    public void run() {
        UI ui = new UI();
        int opcion;
        do {
            opcion = ui.crearMenu();
            ui.ejecutarMenu(opcion);
        } while (opcion != 0);

    }
}

